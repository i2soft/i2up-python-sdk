<START-----------------------------------------------------------
Method: checkBkPath
body: {
 "bk_uuid": "",
 "_": "3e9d77ce"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: createRepBackup
body: {
 "rep_backup": {
  "mirr_sync_attr": 1,
  "cdp_path": "E:/test3/",
  "oph_path": "E:/test4/",
  "secret_key": "",
  "rep_name": "rep_backup",
  "snapshot_policy": 0,
  "bk_path_policy": 1,
  "cdp_process_time": "05:07:28",
  "mirr_open_type": 0,
  "compress": 0,
  "cdp_switch": 1,
  "snapshot_start": 1546913351,
  "cdp_baseline_format": 0,
  "cdp_bl_bkup_switch": 0,
  "encrypt_switch": 0,
  "auto_start": 1,
  "disk_limit": "0",
  "wk_path": [
   "E:/test/"
  ],
  "band_width": "",
  "snapshot_limit": 24,
  "mirr_sync_flag": 0,
  "bk_path": [
   "E:/test2/"
  ],
  "wk_uuid": "B8566905-411E-B2CD-A742-77B1346D8E84",
  "mirr_file_check": 0,
  "cdp_bl_sched_switch": 1,
  "del_policy": 1,
  "cmp_switch": 0,
  "rep_type": 0,
  "snapshot_interval": 1,
  "file_type_filter_switch": 0,
  "snapshot_switch": 1,
  "file_type_filter": "",
  "cdp_param": "3,30,0",
  "oph_policy": 2,
  "mirr_skip": "0",
  "bk_uuid": "B8566905-411E-B2CD-A742-77B1346D8E84",
  "cdp_bl_sched": "2|1|0|5",
  "excl_path": [],
  "mirr_sched": "",
  "bkup_one_time": 1515568566,
  "mirr_sched_switch": 0,
  "cdp_snap_on": 0,
  "cdp_snap_interval": 30,
  "cdp_snap_count": 240,
  "ct_name_type": 0,
  "ct_name_str1": "",
  "ct_name_str2": "",
  "ct_name_str3": "",
  "ct_name_str4": "",
  "cmp_file_check": 0,
  "cmp_schedule": [
   {
    "sched_every": 1,
    "sched_time": [
     "08:26"
    ],
    "sched_day": [
     "7"
    ]
   }
  ],
  "thread_num": "0",
  "cdp_zfs_pool": "",
  "cdp_data_inc_switch": 0,
  "cdp_data_inc": 0,
  "cdp_data_inc_flag": "",
  "latency_threshold": 1,
  "mscs_autostart": 1,
  "mir_detect_script": "",
  "mscs_group": {},
  "filter_delete": 0,
  "cmp_limit": "",
  "data_ip_uuid": "B8166905-411E-B2CD-A742-77B1346D8E84",
  "bk_file_crypt": 0,
  "mir_detect_src_script": "",
  "bk_crypt_type": 1,
  "bk_crypt_key": "",
  "traversing_sync": 1,
  "encrypt": 1,
  "compress_switch": 1,
  "rep_uuid": "B8166905-411E-B2CD-A742-77B1346D8E84",
  "cdp_path_switch": 1,
  "pool_uuid": ""
 },
 "_": "3e2568cc"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  The rep_backup[thread_num] field must contain a number greater than or equal to 1;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteRepBackup
body: {
 "rep_uuids": [
  "11111111-1111-1111-1111-111111111111"
 ],
 "force": 1,
 "del_policy": 0,
 "recycle": 0,
 "_": "3e7b97c7"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010031017,
  "message": "[1010031017] Rule does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteRepBackupBaseline
body: {
 "cdp_time_list": "2017-11-17 17:24:14",
 "_": "3f63556d"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001021,
  "message": "[1010001021] Item does not enable this feature."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteRepBackupOrphan
body: {
 "path": "/",
 "orphan_list": [
  ""
 ],
 "_": "3ebd1d1d"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: orphan_list[0];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteRepBackupSnapshot
body: {
 "snapshot_names": [],
 "_": "3da7b3f6"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: snapshot_names[0];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeRepBackup
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: downloadRepBackupOrphan
body: {
 "orphan": "",
 "path": "/",
 "_": "3e165ec2"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: orphan;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: getRepBackupCdpSnapNum
body: {
 "bk_uuid": "",
 "cdp_zfs_pool": "",
 "_": "3f46d99f"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: bk_uuid;\n Lost: cdp_zfs_pool;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listRepBackup
body: {
 "search_value": "",
 "limit": 15,
 "type": 1,
 "page": 1,
 "search_field": "",
 "status": "",
 "_": "3f151e7f"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listRepBackupBaseLine
body: {
 "page": 1,
 "limit": 10,
 "rep_uuid": "",
 "rc_method": 0,
 "data_path": "",
 "bk_uuid": "",
 "_": "3f0158f3"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: rep_uuid;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listRepBackupCdpZfs
body: {
 "bk_uuid": "",
 "_": "3da855c9"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: bk_uuid;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listRepBackupMscsGroup
body: {
 "node_uuid": "",
 "_": "3f23ebd3"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: node_uuid;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listRepBackupOrphan
body: {
 "path": "",
 "_": "3f582c17"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001021,
  "message": "[1010001021] Item does not enable this feature."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listRepBackupSnapshot
body: {
 "page": 1,
 "limit": 10,
 "rc_method": "",
 "rep_uuid": "",
 "bk_uuid": "",
 "data_path": "",
 "_": "3f23cc53"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: rc_method;\n Lost: rep_uuid;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listRepBackupStatus
body: {
 "force_refresh": 1,
 "_": "3dc4b08d"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "status": []
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyRepBackup
body: {
 "rep_backup": {
  "cdp_param": "",
  "rep_type": 0,
  "bkup_one_time": 1515568566,
  "snapshot_switch": 0,
  "cdp_baseline_format": "",
  "mirr_sync_flag": "0",
  "mirr_open_type": "0",
  "auto_start": "1",
  "snapshot_policy": "0",
  "cdp_bl_sched_switch": 0,
  "snapshot_interval": "0",
  "bk_path": "D:/DataTest2/",
  "snapshot_start": 1515568566,
  "random_str": "0DD4E727-70AB-62C6-BEB5-D012DFAE46E3",
  "cdp_path": "",
  "file_type_filter_switch": 0,
  "cmp_schedule": {
   "sched_time": "15:32",
   "sched_day": 2,
   "sched_every": 2
  },
  "snapshot_limit": "24",
  "cmp_switch": 0,
  "oph_path": "",
  "secret_key": "",
  "excl_path": [],
  "schedule": "",
  "policy_interval": 1,
  "cdp_switch": "",
  "wk_uuid": "0DD4E727-70AB-62C6-BEB5-D012DFAE46E3",
  "policy_operation": 1,
  "wk_path": "D:/DataTest/",
  "mirr_skip": "0",
  "policy_limit": 1,
  "cdp_bl_sched": "",
  "del_policy": "1",
  "mirr_sched": "3*03:00-14:00,2*02:00-15:00",
  "encrypt_switch": "0",
  "band_width": "3*03:00-14:00*2m,2*02:00-15:00*80m",
  "compress": "0",
  "mirr_sync_attr": "1",
  "policy_start": 1,
  "cdp_process_time": "",
  "bk_path_policy": "0",
  "cdp_bl_bkup_switch": 0,
  "file_type_filter": "",
  "disk_limit": "0",
  "oph_policy": "0",
  "mirr_file_check": "0",
  "cmp_file_check": 0,
  "mirr_sched_switch": 0,
  "thread_num": "0",
  "cdp_data_inc": 1,
  "cdp_data_inc_switch": 1,
  "cdp_data_inc_flag": "",
  "mscs_autostart": 1,
  "mir_detect_script": "",
  "filter_delete": 0,
  "batch_adv_switch": 1,
  "batch_encrypt_switch": 1,
  "batch_mirr_switch": "",
  "batch_switch": 1,
  "batch_cdp_switch": "",
  "batch_cmp_switch": "",
  "cmp_limit": "",
  "data_ip_uuid": "B8166905-411E-B2CD-A742-77B1346D8E84",
  "compress_switch": 1
 },
 "_": "3f49369f"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010110011,
  "message": "[1010110011] The node has no license associated with this feature. UUID: 0DD4E727-70AB-62C6-BEB5-D012DFAE46E3"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: repBackup
body: {
 "limit": 10,
 "type": 0,
 "page": 1,
 "_": "3f535f52"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: repBackupVerifyDevice
body: {
 "node_uuid": "",
 "dir_name": "",
 "_": "3e284de3"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: node_uuid;\n Lost: dir_name;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: batchCreateRepBackup
body: {
 "base_info_list": {
  "mirr_sync_attr": "1",
  "cdp_path": "E: est3/",
  "oph_path": "E: est4/",
  "secret_key": "",
  "rep_prefix": "bk_",
  "snapshot_policy": "0",
  "bk_path_policy": "1",
  "cdp_process_time": "05:07:28",
  "mirr_open_type": "0",
  "compress": "0",
  "cdp_switch": "1",
  "snapshot_start": 1546913351,
  "cdp_baseline_format": "0",
  "cdp_bl_bkup_switch": 0,
  "encrypt_switch": "0",
  "auto_start": "1",
  "disk_limit": "0",
  "band_width": "",
  "snapshot_limit": "24",
  "mirr_sync_flag": "0",
  "mirr_file_check": "0",
  "cdp_bl_sched_switch": 1,
  "del_policy": "1",
  "cmp_switch": 0,
  "rep_type": 0,
  "snapshot_interval": "1",
  "file_type_filter_switch": 0,
  "snapshot_switch": 1,
  "file_type_filter": "",
  "cdp_param": "3,30,0",
  "oph_policy": "2",
  "mirr_skip": "0",
  "cdp_bl_sched": "2|1|0|5",
  "mirr_sched": "",
  "bkup_one_time": 1515568566,
  "mirr_sched_switch": 0,
  "cdp_snap_on": 0,
  "cdp_snap_interval": 30,
  "cdp_snap_count": 240,
  "ct_name_type": 0,
  "ct_name_str1": "",
  "ct_name_str2": "",
  "ct_name_str3": "",
  "ct_name_str4": "",
  "cmp_file_check": 0,
  "cmp_schedule": [
   {
    "sched_every": 1,
    "sched_time": [
     "20:06"
    ],
    "sched_day": [
     "11"
    ]
   }
  ],
  "thread_num": "0",
  "cdp_zfs_pool": "",
  "cdp_data_inc_switch": 0,
  "cdp_data_inc": 0,
  "cdp_data_inc_flag": "",
  "latency_threshold": 1,
  "mscs_autostart": 1,
  "mir_detect_script": "",
  "mscs_group": {},
  "rep_sufix": "",
  "variable_type": "node",
  "batch_name": "",
  "compress_switch": 0
 },
 "rep_backup": [
  {
   "wk_uuid": "",
   "bk_uuid": "",
   "wk_path": [],
   "bk_path": [],
   "excl_path": []
  }
 ],
 "_": "3f3a9ab9"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: base_info_list[cdp_path_switch];\n Lost: base_info_list[encrypt];\n The base_info_list[thread_num] field must contain a number greater than or equal to 1;\n Lost: rep_backup[0][wk_path][0];\n Lost: rep_backup[0][wk_uuid];\n Lost: rep_backup[0][bk_uuid];\n Lost: rep_backup[0][bk_path][0];\n"
 }
}
-----------------------------------------------------------END>

