<START-----------------------------------------------------------
Method: avoidAlert
body: {
 "lic_uuids": [],
 "_": "3dceeadd"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: cdmCapacity
body: {
 "page": 1,
 "limit": 1,
 "_": "3e8711c8"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "lic_list": [],
  "storage_list": [],
  "total": 0,
  "total_num": 0,
  "total_used": 0,
  "total_authorized": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeActivateInfo
body: {
 "group_sn": "20-4570098558",
 "_": "3f6637d6"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeVpAuthDetail
body: {
 "page": 1,
 "limit": 1,
 "_": "3d3d6d62"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0,
  "lic_list": [],
  "vp_vm_num": 0,
  "vp_vm_num_used": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: getNodeListForLicense
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: hdfsCapacity
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "lic_list": [],
  "platform_list": [],
  "total_num": 0,
  "total_used": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listLicAlert
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "lic_list": []
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listLicBackupAuthDetail
body: {
 "page": "",
 "limit": "",
 "_": "3efe908b"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "backup9_detail": {
   "lic_list": [],
   "total": 0,
   "backend_task": {
    "id": 18,
    "type": 0,
    "trigger_method": "cron/check_license_capacity_used",
    "interval": 3600,
    "enable": 1,
    "result": "{\"total\":1,\"failed\":0}",
    "last_fire_time": 1737875271,
    "status": 0
   },
   "capacity_used_info": {
    "total": 0,
    "disk": 0,
    "storage_pool": 0,
    "dedupe_pool": 0,
    "tape": 0,
    "nas": 0,
    "object": 0,
    "iso": 0,
    "capacity": 0,
    "record_time": 1737875271
   },
   "obj_list": [],
   "func_list": []
  }
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listNearExpirationLicenses
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: unsubscribeLic
body: {
 "sn": "",
 "operate": "unsubscribe",
 "_": "3cd53256"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: updateLic
body: {
 "config": {
  "warn_sw": 1,
  "usage_threshold": 1
 },
 "_": "3f3192bb"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

