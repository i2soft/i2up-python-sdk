<START-----------------------------------------------------------
Method: createWebhook
body: {
 "id": "",
 "name": "",
 "type": "",
 "config": {
  "method": "",
  "secret": "",
  "headers": []
 },
 "url": "",
 "_": "3f017b9a"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: id;\n Lost: name;\n Lost: url;\n Lost: type;\n Lost: config[method];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: createWebhookContentTemplate
body: {
 "name": "",
 "warn_type": "",
 "config": {
  "dd|qywx|fs|kafka|generalsms": {
   "language": "zh",
   "title": "",
   "content": "",
   "method": "",
   "count": 1,
   "mentioned_range": 1,
   "mentioned_type": 1,
   "mentioned_list": ""
  }
 },
 "id": "",
 "_": "3d944091"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: id;\n Lost: name;\n Lost: warn_type;\n Lost: config[sms];\n Lost: config[kafka];\n Lost: config[dd];\n Lost: config[qywx];\n Lost: config[fs];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteWebhook
body: {
 "uuids": [],
 "_": "3edf2910"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: uuids[0];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: delteWebhookContentTemplate
body: {
 "uuids": [],
 "_": "3ecdac86"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: uuids[0];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeWebhook
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listWebhook
body: {
 "page": 1,
 "limit": 1,
 "where_args": "{type:}",
 "like_args": "{name:}",
 "_": "3f42d31f"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listWebhookContentTemplate
body: {
 "uuid": "",
 "limit": 1,
 "page": 1,
 "like_args": "{name:}",
 "where_args": "{warn_type:}",
 "_": "3e10e6b5"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyWebhook
body: {
 "name": "",
 "random_str": "",
 "config": {},
 "url": "",
 "id": "",
 "_": "3ee04915"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: id;\n Lost: name;\n Lost: url;\n Lost: type;\n Lost: config[method];\n Lost: random_str;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyWebhookContentTemplate
body: {
 "id": "",
 "name": "",
 "warn_type": "",
 "webhook_type": "",
 "random_str": "",
 "config": {
  "language": "",
  "title": "",
  "content": ""
 },
 "_": "3ceebe8d"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: id;\n Lost: name;\n Lost: warn_type;\n Lost: config[sms];\n Lost: config[kafka];\n Lost: config[dd];\n Lost: config[qywx];\n Lost: config[fs];\n Lost: random_str;\n"
 }
}
-----------------------------------------------------------END>

