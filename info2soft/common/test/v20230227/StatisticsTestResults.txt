<START-----------------------------------------------------------
Method: describeStatistics
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "",
  "list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: downloadStatistics
body: {
 "page": 1,
 "end": 1,
 "name": "",
 "limit": 10,
 "start": 1,
 "status": "",
 "type": "",
 "result": 1,
 "group_uuid": "",
 "uuid": "",
 "statistics_start": 1,
 "statistics_end": 1,
 "src_type": 1,
 "obj_name": "",
 "time_consuming": 1,
 "wk_uuid": "",
 "bk_uuid": "",
 "other_uuid": "",
 "suffix": ".csv",
 "_": "3ef31e81"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: downloadStatisticsChart
body: {
 "start": 1,
 "sub_type": "0",
 "end": 2,
 "type": "vp",
 "page": 1,
 "limit": 10,
 "timing_only": 0,
 "sys_name": "",
 "protect_name": "",
 "suffix": "",
 "_": "3ebafd34"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "",
  "list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listStatistics
body: {
 "page": 1,
 "end": 1,
 "name": "",
 "limit": 10,
 "start": 1,
 "status": "",
 "type": "",
 "result": 1,
 "group_uuid": "",
 "uuid": "",
 "time_used_rate": 1,
 "sub_type": 1,
 "obj_name": "",
 "time_consuming": 1,
 "wk_uuid": "",
 "bk_uuid": "",
 "other_uuid": "",
 "sys_name": "",
 "protect_name": "",
 "_": "3f77335a"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  The start field must be exactly 10 characters in length;\n The end field must be exactly 10 characters in length;\n The sub_type field must be one of: bak_bk,bak_rc,cmp_all,ffo,sys_cdm,rule,vp,sys_cloud,bb,DB2,MSSQL,MySQL,GaussDB,GaussDB100T,Informix,DM,block,file,Oracle,GoldenDB,cmp,cmp_nas,ffo_bk,ffo_rc,ffo_or,sys_cdm_bk,sys_cdm_rc,cdm_or,rule_rc,rule_rep,vp_bk,vp_mv,vp_rc,vp_pt,vp_or,ha,ha_switch,sys_cloud_bk,bb_bk,bb_rc,dto,dto_bk,dto_rc,dto_cmp,cdm_remote_rep,zfs_pool,zfs_volume,zfs_filesystem,zfs_obj;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listStatisticsChart
body: {
 "start": 1,
 "sub_type": "0",
 "end": 2,
 "type": "vp",
 "page": 1,
 "limit": 10,
 "timing_only": 0,
 "sys_name": "",
 "protect_name": "",
 "_": "3f691027"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  The sub_type field must be one of: bak_bk,bak_rc,cmp_all,ffo,sys_cdm,rule,vp,sys_cloud,bb,DB2,MSSQL,MySQL,GaussDB,GaussDB100T,Informix,DM,block,file,Oracle,GoldenDB,cmp,cmp_nas,ffo_bk,ffo_rc,ffo_or,sys_cdm_bk,sys_cdm_rc,cdm_or,rule_rc,rule_rep,vp_bk,vp_mv,vp_rc,vp_pt,vp_or,ha,ha_switch,sys_cloud_bk,bb_bk,bb_rc,dto,dto_bk,dto_rc,dto_cmp,cdm_remote_rep,zfs_pool,zfs_volume,zfs_filesystem,zfs_obj;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listStatisticsConfig
body: {
 "_": "3e4e448d"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "statistics_report_conf": null
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: updateStatisticsConfig
body: {
 "daily_report": {
  "daily_sw": 1,
  "daily_st": "11:43",
  "OVERVIEW": 1,
  "bak_bk": 1,
  "bak_rc": 1,
  "cmp_all": 1,
  "ffo": 1,
  "rule": 1,
  "vp": 1,
  "ha": "",
  "sys_cdm": "",
  "bb": "",
  "sys_cloud": "",
  "cdm_remote_rep": ""
 },
 "weekly_report": {
  "weekly_sw": "",
  "weekly_st": "1,00:00",
  "OVERVIEW": 1,
  "bak_bk": 1,
  "bak_rc": 1,
  "cmp_all": 1,
  "ffo": 1,
  "rule": 1,
  "vp": 1,
  "ha": "",
  "sys_cdm": "",
  "bb": "",
  "sys_cloud": "",
  "cdm_remote_rep": ""
 },
 "monthly_report": {
  "monthly_sw": "",
  "monthly_st": "1,00:00",
  "OVERVIEW": 1,
  "bak_bk": 1,
  "bak_rc": 1,
  "cmp_all": 1,
  "ffo": 1,
  "rule": 1,
  "vp": 1,
  "ha": "",
  "sys_cdm": "",
  "bb": "",
  "sys_cloud": "",
  "cdm_remote_rep": ""
 },
 "email": "g.ntooytmk@ftgxjmb.edu",
 "realtime_report": {
  "realtime_sw": 1,
  "bak_bk": 1,
  "bak_rc": 1,
  "cmp_all": 1,
  "ffo": 1,
  "rule": 1,
  "vp": 1,
  "ha": 1,
  "sys_cdm": 1,
  "bb": "",
  "sys_cloud": "",
  "sms_content": "",
  "cdm_remote_rep": ""
 },
 "hourly_report": {
  "hourly_sw": "",
  "hourly_st": "\"00\"",
  "OVERVIEW": "",
  "bak_bk": "",
  "bak_rc": "",
  "cmp_all": "",
  "ffo": "",
  "rule": "",
  "vp": "",
  "ha": "",
  "sys_cdm": "",
  "bb": "",
  "sys_cloud": "",
  "cdm_remote_rep": ""
 },
 "stat_type": "0",
 "phone": "",
 "sms_template": "",
 "sms_switch": "",
 "sms_report_template": "",
 "email_switch": "",
 "_": "3f4e1143"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

