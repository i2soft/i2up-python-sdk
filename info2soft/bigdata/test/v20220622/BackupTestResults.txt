<START-----------------------------------------------------------
Method: createBigdataBackup
body: {
 "bigdata_backup": {
  "rule_name": "",
  "rule_uuid": "",
  "bk_uuid": "F85DFEC0-149E-373D-0B9E-3DA9A5C43940",
  "bk_path": [],
  "baked_paths": [],
  "data_type": "",
  "cred_switch": 1,
  "cred_uuid": "",
  "auth_user": "",
  "auth_key": "",
  "mirr_file_check": 0,
  "mirr_sync_flag": 1,
  "bkup_one_time": 0,
  "bkup_policy": 2,
  "bkup_schedule": [
   {
    "sched_day": 5,
    "sched_time": "22:48",
    "sched_every": 2,
    "limit": 16,
    "backup_type": 0,
    "policys": "\u6bcf\u592922:00\u81ea\u52a8\u6267\u884c",
    "backup_type_show": "\u5168\u5907",
    "running_time": "22:00"
   }
  ],
  "random_str": "11111111-1111-1111-1111-111111111111"
 },
 "_": "3ea47098"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: bigdata_backup[rule_name];\n Lost: bigdata_backup[data_type];\n Lost: bigdata_backup[cred_uuid];\n Lost: bigdata_backup[cluster_config_path];\n Lost: bigdata_backup[hive_bktype];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteBigdataBackup
body: {
 "uuids": [
  "11111111-1111-1111-1111-111111111111"
 ],
 "del_policy": 0,
 "_": "3f16dc75"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeBigdataBackup
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listBigdataBackup
body: {
 "search_field": "",
 "limit": 10,
 "page": 1,
 "search_value": "",
 "type": 0,
 "_": "3f33a2d4"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listBigdataBackupStatus
body: {
 "_": "3f50069b"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: uuids[0];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: startBigdataBackup
body: {
 "operate": "start",
 "uuids": [
  "22D03E06-94D0-5E2C-336E-4BEEC2D28EC4"
 ],
 "_": "3e69d7fe"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist",
  "fail_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ],
  "all_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ]
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: startImmediatelyBigdataBackup
body: {
 "operate": "start_immediately",
 "uuids": [
  "22D03E06-94D0-5E2C-336E-4BEEC2D28EC4"
 ],
 "_": "3ec86d5f"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist",
  "fail_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ],
  "all_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ]
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: stopBigdataBackup
body: {
 "operate": "stop",
 "uuids": [
  "22D03E06-94D0-5E2C-336E-4BEEC2D28EC4"
 ],
 "_": "3e74ca50"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist",
  "fail_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ],
  "all_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ]
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listAllBigdataHiveDatabase
body: {
 "bk_uuid": "62bbEE07-eEe8-EffF-d9cf-8ac9d7edcED1",
 "cluster_config_path": "",
 "_": "3d0af2e1"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: cluster_config_path;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listBigdataHiveTable
body: {
 "bk_uuid": "74e8C03A-5bAe-69Fe-2825-4E4DBF57D89F",
 "table_name": "",
 "limit": "",
 "page": "",
 "db_name": "",
 "cluster_config_path": "",
 "_": "3f3d165e"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: db_name;\n"
 }
}
-----------------------------------------------------------END>

