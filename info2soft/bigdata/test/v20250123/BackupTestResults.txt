<START-----------------------------------------------------------
Method: authBigdataPlatform
body: {
 "auth_key": "",
 "auth_name": "",
 "cred_uuid": "",
 "bk_uuid": "",
 "_": "3cc59c17"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: bk_uuid;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: createBigdataBackup
body: {
 "bigdata_backup": {
  "rule_name": "",
  "rule_uuid": "",
  "bk_path": [],
  "baked_paths": [],
  "data_type": "",
  "cred_switch": 1,
  "cred_uuid": "",
  "auth_user": "",
  "auth_key": "",
  "mirr_file_check": 0,
  "mirr_sync_flag": 1,
  "bkup_one_time": 0,
  "bkup_policy": 2,
  "bkup_schedule": [
   {
    "sched_day": 23,
    "sched_time": "01:37",
    "sched_every": 2,
    "limit": 60,
    "backup_type": 0,
    "policys": "\u6bcf\u592922:00\u81ea\u52a8\u6267\u884c",
    "backup_type_show": "\u5168\u5907",
    "running_time": "22:00"
   }
  ],
  "random_str": "11111111-1111-1111-1111-111111111111",
  "tape_uuid": "E8566905-411E-B2CD-A742-77B1346D8E84",
  "archive_pen": 0,
  "library_sn": "SYZZY_A",
  "sel_tbl": [],
  "sel_db": [],
  "hive_bktype": 1,
  "filter_type": 1,
  "filter_files": "",
  "exclude_paths": "",
  "band_width": "",
  "pre_backup_script": "",
  "post_backup_script": "",
  "script_timeout": 1,
  "tape_pool_uuid": "",
  "tape_pool_name": "",
  "tape_name": "",
  "tape_reserve": 1,
  "platform_uuid": "",
  "sel_part": [],
  "rule_type": "",
  "schedule_uuid": "",
  "scan_path": "",
  "schedule": {
   "type": "",
   "interval": "",
   "unit": "",
   "run_time": "12:00"
  },
  "scan_schedule": {
   "sched_every": "",
   "sched_day": "",
   "sched_time": ""
  },
  "approver_uuid": ""
 },
 "obs_settings": {
  "sto_uuid": "",
  "bucket_name": "",
  "bucket_path": ""
 },
 "_": "3ed17643"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: bigdata_backup[rule_name];\n Lost: bigdata_backup[bk_uuid];\n Lost: bigdata_backup[data_type];\n Lost: bigdata_backup[cred_uuid];\n Lost: bigdata_backup[bk_type];\n The bigdata_backup[script_timeout] field must contain a number greater than or equal to 60;\n Lost: bigdata_backup[rule_type];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteBigdataBackup
body: {
 "uuids": [
  "11111111-1111-1111-1111-111111111111"
 ],
 "del_policy": 0,
 "force": 1,
 "_": "3d9d92b6"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeBigdataBackup
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: disableBigdataBackup
body: {
 "operate": "",
 "uuids": "[C6335F62-2565-1957-4BB9-587F2FF46B00]",
 "bk_type": 1,
 "_": "3e90b7c5"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: operate;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: enableBigdataBackup
body: {
 "operate": "",
 "uuids": "[C6335F62-2565-1957-4BB9-587F2FF46B00]",
 "bk_type": 1,
 "_": "3f4a8358"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: operate;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: getBigdataBackupPartitions
body: {
 "bk_uuid": "",
 "db_name": "",
 "table_name": "",
 "limit": "",
 "page": "",
 "platform_uuid": "",
 "_": "3ead4e3d"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: bk_uuid;\n Lost: db_name;\n Lost: table_name;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: importBigdataBackup
body: {
 "rule_uuid": "",
 "file": "",
 "_": "3e1576c0"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listAllBigdataHiveDatabase
body: {
 "bk_uuid": "D2ffB1Fe-2251-Da34-BEB5-7ED7dc4Bf52b",
 "cluster_config_path": "",
 "platform_uuid": "",
 "_": "3e00f9c7"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010011001,
  "message": "[1010011001] Node does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listBigdataBackup
body: {
 "search_field": "",
 "limit": 10,
 "page": 1,
 "search_value": "",
 "type": 0,
 "_": "3eec8c8e"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listBigdataBackupStatus
body: {
 "uuids": [],
 "force_refresh": 1,
 "_": "3e727ee2"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: uuids[];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listBigdataBackupTableDdl
body: {
 "bk_path": [],
 "bk_node_uuid": "",
 "bk_rule_uuid": "",
 "cluster_config_path": "",
 "obs_settings": "{sto_uuid:,bucket:}",
 "bk_type": "",
 "rec_time": "",
 "table_name": "",
 "_": "3f7b0e0c"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010011001,
  "message": "[1010011001] Node does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listBigdataHiveTable
body: {
 "bk_uuid": "9E2e888B-9afC-DebB-2557-D1B4DE6FAf3D",
 "table_name": "",
 "limit": "",
 "page": "",
 "db_name": "",
 "cluster_config_path": "",
 "platform_uuid": "",
 "_": "3f3e60f9"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: db_name;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: pauseBigdataBackup
body: {
 "operate": "",
 "uuids": "[C6335F62-2565-1957-4BB9-587F2FF46B00]",
 "bk_type": 1,
 "_": "3f37205b"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: operate;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: resumeBigdataBackup
body: {
 "operate": "",
 "uuids": "[C6335F62-2565-1957-4BB9-587F2FF46B00]",
 "bk_type": 1,
 "_": "3f5be9f1"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: operate;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: startBigdataBackup
body: {
 "operate": "",
 "uuids": "[C6335F62-2565-1957-4BB9-587F2FF46B00]",
 "bk_type": 1,
 "_": "3e274170"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: operate;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: startImmediatelyBigdataBackup
body: {
 "operate": "",
 "uuids": "[C6335F62-2565-1957-4BB9-587F2FF46B00]",
 "bk_type": 1,
 "_": "3e933a13"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: operate;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: stopBigdataBackup
body: {
 "operate": "",
 "uuids": "[C6335F62-2565-1957-4BB9-587F2FF46B00]",
 "bk_type": 1,
 "_": "3eaaf505"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: operate;\n"
 }
}
-----------------------------------------------------------END>

