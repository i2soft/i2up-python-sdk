<START-----------------------------------------------------------
Method: createNodeProxy
body: {
 "proxy_addr": "",
 "proxy_port": "",
 "_": "3eabf557"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: proxy_addr;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteNodeProxy
body: {
 "uuids": [],
 "_": "3ebe59f1"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeNodeProxy
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listNodeProxy
body: {
 "search_field": "",
 "search_value": "",
 "_": "3e987f9d"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyNodeProxy
body: {
 "proxy_addr": "",
 "proxy_port": "",
 "_": "3e3e176d"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

