<START-----------------------------------------------------------
Method: createBackupDestination
body: {
 "name": "",
 "sto_uuid": "",
 "s3_real_address": 1,
 "bucket": "",
 "region": "",
 "check_sto_address": 1,
 "sto_address_cert": "",
 "cls_uuid": "",
 "copy_switch": 1,
 "src_uuid": "",
 "src_cls_uuid": "",
 "_": "3e6bd676"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The name field is required."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: createCallbackSettings
body: {
 "name": "",
 "type": 1,
 "cls_uuid": "",
 "pod": "",
 "before_backup": [
  {
   "cmd": "",
   "container": "",
   "error_handling": "",
   "timeout": ""
  }
 ],
 "after_backup": [
  {
   "cmd": "",
   "container": "",
   "error_handling": "",
   "timeout": ""
  }
 ],
 "init_container": [
  {
   "name": "",
   "mirror": "",
   "cmd": "",
   "volume": ""
  }
 ],
 "recovery_callback": [
  {
   "container": "",
   "cmd": "",
   "error_handling": "",
   "exec_timeout": 1,
   "wait_timeout": 1
  }
 ],
 "_": "3c6fbe07"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The name field is required."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: createContainerCluster
body: {
 "cls_name": "",
 "cls_config": "",
 "component_settings": {
  "cls_component_config": {
   "cpu_limit": 1,
   "cpu_request": 1,
   "mem_limit": 1,
   "mem_request": 1
  },
  "pv_component_config": {
   "cpu_limit": 1,
   "cpu_request": 1,
   "mem_limit": 1,
   "mem_request": 1
  }
 },
 "component_version": "",
 "cls_version": "",
 "cls_type": 1,
 "component_namespace": "",
 "os_user": "",
 "os_pwd": "",
 "net_type": "",
 "_": "3e794038"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The cls name field is required."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteBackupDestination
body: {
 "uuids": [],
 "_": "3dc70016"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The uuids field is required."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteCallbackSettings
body: {
 "uuids": [],
 "_": "3f7996d0"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The uuids field is required."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteContainerCluster
body: {
 "cls_uuids": [],
 "force": 1,
 "_": "3ec2bb9f"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The uuids field is required."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: descibeBackupDestination
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeCallbackSettings
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeContainerCluster
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listBackupDestination
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listBackupDestinationStatus
body: {
 "uuids": "",
 "force_refresh": "",
 "_": "3f4246fb"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The uuids field is required."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listCallbackSettings
body: {
 "where_args[type]": 1,
 "_": "3d0a50ba"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listContainerClsNamespace
body: {
 "cls_uuid": "58ECEEB7-D4FC-4746-A507-AA3BBC98EFD1",
 "_": "3e4f66f0"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010400000,
  "message": "[1010400000] Container cluster does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listContainerCluster
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listContainerClusterInfo
body: {
 "cls_config": {},
 "username": "",
 "password": "",
 "component_namespace": "",
 "net_type": "",
 "cls_uuid": "",
 "_": "3f40a7ca"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The cls config field is required."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listContainerClusterResource
body: {
 "cls_uuid": "",
 "_": "3f1aace0"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The cls uuid field is required."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyBackupDestination
body: {
 "uuid": "",
 "name": "",
 "sto_uuid": "",
 "random_str": "",
 "s3_real_address": 1,
 "bucket": "",
 "region": "",
 "check_sto_address": 1,
 "sto_address_cert": "",
 "cls_uuid": "",
 "_": "3e71763b"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The random str field is required."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyCallbackSettings
body: {
 "name": "",
 "type": 1,
 "cls_uuid": "",
 "pod": "",
 "uuid": "E8CdEd75-71ea-AC5E-7E19-ABC24D4B2cfb",
 "user_uuid": "E679EF73-5288-E3C4-9608-B33B47416B87",
 "username": "admin",
 "random_uuid": "E679EF73-5288-E3C4-9608-B33B47416B87",
 "before_backup": [
  {
   "cmd": "",
   "container": "",
   "error_handling": "",
   "timeout": ""
  }
 ],
 "after_backup": [
  {
   "cmd": "",
   "container": "",
   "error_handling": "",
   "timeout": ""
  }
 ],
 "init_container": [
  {
   "name": "",
   "mirror": "",
   "cmd": "",
   "volume": ""
  }
 ],
 "recovery_callback": [
  {
   "container": "",
   "cmd": "",
   "error_handling": "",
   "exec_timeout": 1,
   "wait_timeout": 1
  }
 ],
 "_": "3eed848f"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The random str field is required."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyContainerCluster
body: {
 "cls_name": "",
 "cls_config": {},
 "component_settings": {
  "cls_component_config": {
   "cpu_limit": 1,
   "cpu_request": 1,
   "mem_limit": 1,
   "mem_request": 1
  },
  "pv_component_config": {
   "cpu_limit": 1,
   "cpu_request": 1,
   "mem_limit": 1,
   "mem_request": 1
  }
 },
 "component_version": "",
 "current_context": "",
 "cls_version": "",
 "authorization_info": "",
 "random_str": "",
 "_": "3ea56885"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The random str field is required."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: syncContainerClusterInfo
body: {
 "cls_uuid": "",
 "type": "",
 "_": "3f63b6a5"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The cls uuid field is required."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: verifyCallbackSettingsPod
body: {
 "cls_uuid": "",
 "pod": "",
 "_": "3e398db7"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "The cls uuid field is required."
 }
}
-----------------------------------------------------------END>

