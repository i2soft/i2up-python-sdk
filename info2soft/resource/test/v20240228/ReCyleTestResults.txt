<START-----------------------------------------------------------
Method: cleanRecycle
body: {
 "uuids": [],
 "operate": "clean",
 "_": "3dc3cf34"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: uuids[];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteRecycle
body: {
 "uuids": [],
 "_": "3d853bb3"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: uuids[];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listRecycleStatus
body: {
 "force_refresh": 1,
 "_": "3f4543e8"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Contains illegal UUID: uuids[];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listRecycle
body: {
 "page": 1,
 "limit": 1,
 "type": "",
 "_": "3f2d290a"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

