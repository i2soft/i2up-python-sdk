<START-----------------------------------------------------------
Method: createDedupePool
body: {
 "pool_name": "",
 "node_uuid": "",
 "server_port": 1,
 "time_out": 1,
 "block_size": 1,
 "slice_size": 1,
 "hash_path": [],
 "index_path": [],
 "data_path": [],
 "compress": 1,
 "encrypt": 1,
 "secret_key": "",
 "encrypt_switch": 0,
 "ssd_mode": 0,
 "_": "3e315017"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: pool_name;\n Lost: node_uuid;\n Lost: hash_path[0];\n Lost: index_path[0];\n Lost: data_path[0];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: dedupePoolList
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteDedupePool
body: {
 "pool_uuids": [],
 "force": 1,
 "del_data": 1,
 "_": "3ed56f38"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: pool_uuids[0];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeDedupePool
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listDedupePoolStatus
body: {
 "force_refresh": 1,
 "_": "3ef9779b"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: pool_uuids[0];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyDedupePool
body: {
 "pool_uuid": "",
 "random_str": "",
 "index_path": [],
 "data_path": [],
 "_": "3f67294b"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: operateDedupePool
body: {
 "pool_uuids": [],
 "operate": "",
 "_": "3e91aa6e"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: pool_uuids[0];\n Lost: operate;\n"
 }
}
-----------------------------------------------------------END>

