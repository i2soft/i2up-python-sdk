<START-----------------------------------------------------------
Method: createDtoStorageBucket
body: {
 "sto_uuid": "",
 "bucket_name": "",
 "quota_size": "",
 "_": "3f0e7a8a"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: bucket_name;\n Lost: sto_uuid;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteDtoStorageBucket
body: {
 "bucket_uuids": [
  "11111111-1111-1111-1111-111111111111"
 ],
 "_": "3f6c062d"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it.",
  "fail_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
   }
  ],
  "all_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
   }
  ]
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeDtoStorageBucket
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: importDtoStorageBucket
body: {
 "bucket_names": [],
 "sto_uuid": "",
 "_": "3eef3648"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: sto_uuid;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listDtoStorageBucket
body: {
 "limit": 1,
 "search_value": "",
 "search_field": "",
 "page": 10,
 "_": "3f48db50"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

