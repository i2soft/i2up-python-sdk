<START-----------------------------------------------------------
Method: createDtoLm
body: {
 "rule_name": "",
 "status": 0,
 "type": 1,
 "prefix": "",
 "lfa_stor": {
  "config_sw": 1,
  "days": 1
 },
 "arch_stor": {
  "config_sw": 1,
  "days": 1
 },
 "expr_del": {
  "config_sw": 1,
  "days": 1
 },
 "sto_uuid": "",
 "host_uuid": "",
 "path": "",
 "rule_id": "",
 "_": "3f33c528"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: rule_name;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listDtoLm
body: {
 "sto_uuid": "",
 "host_uuid": "",
 "path": "",
 "rule_name": "",
 "_": "3dd1cab0"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: sto_uuid;\n Lost: host_uuid;\n Lost: path;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyDtoLm
body: {
 "rule_name": "",
 "status": 0,
 "type": 1,
 "prefix": "",
 "lfa_stor": {
  "config_sw": "",
  "days": ""
 },
 "arch_stor": {
  "config_sw": "",
  "days": ""
 },
 "expr_del": {
  "config_sw": "",
  "days": ""
 },
 "sto_uuid": "",
 "host_uuid": "",
 "path": "",
 "_": "3e68b359"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1011110001,
  "message": "[1011110001] RPC call failed or Couldn't connect to target service (node, platform, API)"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: operateDtoLm
body: {
 "type": "",
 "sto_uuid": "",
 "host_uuid": "",
 "path": "",
 "rule_names": [],
 "_": "3f4e5ad9"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: type;\n"
 }
}
-----------------------------------------------------------END>

