<START-----------------------------------------------------------
Method: authDtoHost
body: {
 "host_ip": "192.168.72.70",
 "host_user": "exampleuser",
 "host_pwd": "dN5BejxqJsnEQOBRig7OBeZzQb1SEYAfs0keD+6z1l658pc/drceaMJa29FDdQpW6FfLLmb1cG1DWvOOGz9sZRUY4wnKNhpHQjVE4wAlLOnVZPGlYSgtURhbIOeLl5uZCWgCSGTbQFMTCD/wql4/8/cMgWspQBvwO/5UbYqcW64Sj8wnuWf6qt4KGqrP9ua2yDFj+5S0MgMLWnAXhBwCCFVBmmmngNr5CUMe4Hqm1/d4OhvTzqTWecLNFnr9NmN4fp1zAQMZstUiedgWGg7uU9Aez2Xf8RsekMeo3O7bnZXyHZL5wpOtiq3gD/12H4bNrgDYuShsGDfEEqzfwXpoew==",
 "host_port": "",
 "_": "3f7fe78c"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: host_port;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: createDtoHost
body: {
 "host_name": "",
 "host_ip": "",
 "host_user": "",
 "host_pwd": "",
 "sto_uuid": "CCF36C5F-CBA6-8A55-3CA2-C07CF8E0EC4F",
 "comment": "",
 "cc_ip": "",
 "cc_ip_uuid": "",
 "maintenance": 0,
 "syncdb_host": "",
 "syncdb_username": "",
 "syncdb_password": "",
 "host_type": 1,
 "syncdb_type": 1,
 "node_list": [
  {
   "address": "",
   "port": "",
   "uuid": ""
  }
 ],
 "host_cluster_type": "",
 "syncdb_port": "",
 "syncdb_name": "",
 "host_port": "",
 "archive_db_type": 1,
 "_": "3d5e7936"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: host_name;\n Lost: host_ip;\n Lost: host_port;\n Lost: host_user;\n Lost: host_pwd;\n Lost: cc_ip_uuid;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteDtoHost
body: {
 "force": 1,
 "host_uuids": [],
 "_": "3e8328e5"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: host_uuids[];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteDtoHostClusterNode
body: {
 "node_uuid": "",
 "force": 1,
 "_": "3f2d88f5"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it.",
  "fail_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
   }
  ],
  "all_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
   }
  ]
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeDtoHost
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listArchiveDate
body: {
 "type": 0,
 "_": "3f30590e"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listArchiveFile
body: {
 "data_source": "2019",
 "page": 1,
 "limit": 100,
 "wk_path": "",
 "file_name": "",
 "create_begin_time": 1,
 "create_end_time": 1,
 "modify_begin_time": 1,
 "modify_end_time": 1,
 "_": "3f34034b"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listBakRecord
body: {
 "file_name": "",
 "wk_path": "",
 "bk_path": "",
 "begin_backup_time": 1,
 "end_backup_time": 1,
 "page": "1",
 "limit": "100",
 "_": "3e86999d"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listDtoHost
body: {
 "limit": 1,
 "page": 1,
 "search_value": "",
 "search_field": "",
 "_": "3eb3a307"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listDtoHostClusterNode
body: {
 "where_args": "{host_uuid:}",
 "_": "3d76094f"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listDtoHostStatus
body: {
 "host_uuids": [],
 "force_refresh": 1,
 "_": "3ec72a45"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: host_uuids[];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listLoadRules
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listRcTimePoint
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listRevertRecord
body: {
 "data_source": "2019",
 "page": 1,
 "limit": 100,
 "wk_path": "",
 "file_name": "",
 "create_begin_time": 1,
 "create_end_time": 1,
 "_": "3f4e4ad8"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: maintainDtoHost
body: {
 "host_uuids": [],
 "operate": "",
 "switch": 0,
 "_": "3f71dd21"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: host_uuids[];\n Lost: operate;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyDtoHost
body: {
 "host_uuid": "",
 "host_name": "",
 "host_ip": "",
 "host_user": "",
 "host_pwd": "",
 "sto_uuid": 0,
 "random_str": "",
 "cc_ip": "",
 "cc_ip_uuid": "",
 "maintenance": 0,
 "_": "3c08a594"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: host_name;\n Lost: host_ip;\n Lost: host_port;\n Lost: host_user;\n Lost: host_pwd;\n Not uuid: sto_uuid;\n Lost: cc_ip_uuid;\n Lost: random_str;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: renewKeyDtoHost
body: {
 "host_uuids": [],
 "operate": "",
 "switch": 0,
 "_": "3f55bb41"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: host_uuids[];\n Lost: operate;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: revertFile
body: {
 "ids": [
  {
   "": ""
  }
 ],
 "data_source": "2021",
 "_": "3f161b4e"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: upgradeDtoHost
body: {
 "host_uuids": [],
 "operate": "",
 "switch": 0,
 "_": "3e338ee9"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: host_uuids[];\n Lost: operate;\n"
 }
}
-----------------------------------------------------------END>

