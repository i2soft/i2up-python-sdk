<START-----------------------------------------------------------
Method: createCompare
body: {
 "compare": {
  "excl_path": [],
  "bkup_one_time": 0,
  "bkup_schedule": {
   "sched_gap_min": 60,
   "sched_time": [
    "00:00:00"
   ],
   "sched_day": [
    "1"
   ],
   "sched_time_end": "23:59",
   "limit": 5,
   "sched_time_start": "00:00",
   "sched_every": 0
  },
  "mirr_file_check": "1",
  "task_name": "testCompare1",
  "wk_path": [
   "E:\\test\\"
  ],
  "bk_uuid": "67E33CDB-D75B-15B3-367D-50C764F5A26F",
  "cmp_type": 0,
  "bk_path": [
   "E:\\test\\"
  ],
  "bkup_policy": 2,
  "compress": 0,
  "wk_uuid": "67E33CDB-D75B-15B3-367D-50C764F5A26F",
  "mirr_sync_attr": 1,
  "encrypt_switch": 1,
  "secret_key": "",
  "biz_grp_list": [],
  "oph_policy": "",
  "ct_name_str1": "",
  "ct_name_str2": "",
  "ct_name_str3": "",
  "ct_name_str4": "",
  "data_ip_uuid": "67E33CDB-D75B-15B3-367D-50C764F5A26F",
  "oph_path": "",
  "traversing_sync": 1,
  "band_width": "123*01:00-02:00*2m,34*12:00-13:00*6m",
  "task_type": 1,
  "file_type_filter_switch": 1,
  "file_type_filter": "",
  "compress_switch": 0,
  "pre_work_script": "",
  "pre_back_script": "",
  "post_work_script": "",
  "post_back_script": "",
  "encrypt": 1,
  "script_timeout": 1,
  "script_timeout_every": "",
  "subpath_filter_switch": 1,
  "subpath_filter": ""
 },
 "_": "3f20c8ca"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: compare[mirr_hash_type];\n Lost: compare[oph_policy];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteCompare
body: {
 "task_uuids": [
  "11111111-1111-1111-1111-111111111111"
 ],
 "force": 1,
 "_": "3f244c3e"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it.",
  "fail_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
   }
  ],
  "all_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
   }
  ]
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeCompare
body: {}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] This entry does not exist, try refreshing the page if operating on it."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: downloadCompare
body: {
 "operate": "download",
 "task_uuids": [
  "11111111-1111-1111-1111-111111111111"
 ],
 "_": "3e272b1c"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "file_name": "CPR_20250126164722.zip"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listCircleCompareResult
body: {
 "search_field": "",
 "limit": 10,
 "search_value": "",
 "page": 1,
 "_": "3f74b521"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listCompare
body: {
 "search_value": "",
 "limit": 10,
 "page": 1,
 "search_field": "",
 "type": "",
 "_": "3e7467d9"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listCompareStatus
body: {
 "task_uuids": "task_uuids[]=11111111-1111-1111-1111-111111111111",
 "force_refresh": 1,
 "_": "3f057752"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "status": []
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyCompare
body: {
 "compare": {
  "excl_path": [],
  "bkup_one_time": 0,
  "bkup_schedule": {
   "sched_gap_min": 60,
   "sched_time": [
    "00:00:00"
   ],
   "sched_day": [
    "1"
   ],
   "sched_time_end": "23:59",
   "limit": 5,
   "sched_time_start": "00:00",
   "sched_every": 0
  },
  "mirr_file_check": "1",
  "task_name": "testCompare1",
  "wk_path": [
   "E:\\test\\"
  ],
  "bk_uuid": "67E33CDB-D75B-15B3-367D-50C764F5A26F",
  "cmp_type": 0,
  "bk_path": [
   "E:\\test\\"
  ],
  "bkup_policy": 2,
  "compress": 0,
  "wk_uuid": "67E33CDB-D75B-15B3-367D-50C764F5A26F",
  "mirr_sync_attr": 1,
  "encrypt_switch": "",
  "secret_key": "",
  "biz_grp_list": [],
  "oph_policy": "",
  "ct_name_str1": "",
  "ct_name_str2": "",
  "ct_name_str3": "",
  "ct_name_str4": "",
  "task_uuid": "",
  "random_str": "",
  "data_ip_uuid": "67E33CDB-D75B-15B3-367D-50C764F5A26F",
  "band_width": "",
  "task_type": 1,
  "file_type_filter_switch": "",
  "file_type_filter": "",
  "compress_switch": 0,
  "pre_work_script": "",
  "pre_back_script": "",
  "post_work_script": "",
  "post_back_script": "",
  "subpath_filter_switch": 1,
  "subpath_filter": ""
 },
 "_": "3f425887"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: compare[encrypt_switch];\n Lost: compare[mirr_hash_type];\n Lost: compare[oph_policy];\n Lost: compare[file_type_filter_switch];\n Lost: compare[traversing_sync];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: startCompare
body: {
 "operate": "download",
 "task_uuids": [
  "11111111-1111-1111-1111-111111111111"
 ],
 "_": "3f21b537"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "file_name": "CPR_20250126164724.zip"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: startImmediatelyCompare
body: {
 "operate": "download",
 "task_uuids": [
  "11111111-1111-1111-1111-111111111111"
 ],
 "_": "3f3b7f08"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "file_name": "CPR_20250126164724.zip"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: stopCompare
body: {
 "operate": "download",
 "task_uuids": [
  "11111111-1111-1111-1111-111111111111"
 ],
 "_": "3de1a9ed"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "file_name": "CPR_20250126164724.zip"
 }
}
-----------------------------------------------------------END>

