<START-----------------------------------------------------------
Method: createFspMove
body: {
 "fsp_move": {
  "fsp_name": "linuxMove",
  "service_uuid": "",
  "monitor_type": 0,
  "bk_path": [
   "/",
   "/I2FFO/bin/",
   "/I2FFO/boot/",
   "/I2FFO/etc/",
   "/I2FFO/lib/",
   "/I2FFO/lib64/",
   "/I2FFO/root/",
   "/I2FFO/sbin/",
   "/I2FFO/usr/bin/",
   "/I2FFO/usr/lib/",
   "/I2FFO/usr/lib64/",
   "/I2FFO/usr/libexec/",
   "/I2FFO/usr/local/",
   "/I2FFO/usr/sbin/",
   "/I2FFO/var/lib/nfs/"
  ],
  "compress": "0",
  "net_mapping": [],
  "bk_uuid": "C11FE572-5207-3359-DB85-001E95F5F185",
  "encrypt_switch": "0",
  "mirr_open_type": "0",
  "sync_item": "/",
  "mirr_sync_flag": "0",
  "net_mapping_type": "2",
  "mirr_sync_attr": "1",
  "band_width": "3*03:00-04:00*2m",
  "excl_path": [
   "/cgroup/",
   "/dev/",
   "/etc/X11/xorg.conf/",
   "/etc/init.d/i2node/",
   "/etc/rc.d/init.d/i2node/",
   "/etc/sdata/",
   "/lost+found/",
   "/media/",
   "/mnt/",
   "/proc/",
   "/run/",
   "/selinux/",
   "/sys/",
   "/tmp/",
   "/usr/local/sdata/",
   "/var/i2/",
   "/var/i2data/",
   "/var/lock/",
   "/var/run/vmblock-fuse/"
  ],
  "fsp_wk_shut_flag": "2",
  "secret_key": "",
  "wk_path": [
   "/",
   "/bin/",
   "/boot/",
   "/etc/",
   "/lib/",
   "/lib64/",
   "/root/",
   "/sbin/",
   "/usr/bin/",
   "/usr/lib/",
   "/usr/lib64/",
   "/usr/libexec/",
   "/usr/local/",
   "/usr/sbin/",
   "/var/lib/nfs/"
  ],
  "mirr_file_check": "0",
  "wk_uuid": "CE77F3D6-A6E3-A385-CE66-712313B7DDE8",
  "failover": "2"
 },
 "_": "3efd495a"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: fsp_move[compress_switch];\n The fsp_move[failover] field must be one of: 0,1;\n Lost: fsp_move[wk_data_type];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteFspMove
body: {
 "fsp_uuids": [
  "3C6B932A-9140-7C37-9639-EB8F44654F18"
 ],
 "_": "3d29eefa"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: force;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeFspMove
body: {
 "fsp_move": {
  "fsp_uuid": "3C6B932A-9140-7C37-9639-EB8F44654F18"
 }
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listFspMove
body: {
 "search_field": "",
 "limit": 1,
 "page": 1,
 "search_value": "",
 "_": "3f33ebf1"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listFspMoveDir
body: {
 "fsp_uuid": "",
 "wk_uuid": "CE77F3D6-A6E3-A385-CE66-712313B7DDE8",
 "_": "3d37d5ef"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010011001,
  "message": "[1010011001] Node does not exist CE77F3D6-A6E3-A385-CE66-712313B7DDE8",
  "hd_list": [],
  "dir_list": null,
  "os_type": null
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listFspMoveDriverInfo
body: {
 "wk_uuid": "",
 "_": "3df60e7e"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010061101,
  "message": "[1010061101] Fail to connect work node.",
  "driver_list": []
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listFspMoveNic
body: {
 "bk_uuid": "C11FE572-5207-3359-DB85-001E95F5F185",
 "wk_uuid": "CE77F3D6-A6E3-A385-CE66-712313B7DDE8",
 "_": "3f13a3a3"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "wk_nic_list": 1011110001,
  "bk_nic_list": 1011110001
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listFspMoveStatus
body: {
 "_": "3e55584c"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "status": []
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyFspMove
body: {
 "fsp_move": {
  "fsp_uuid": "3C6B932A-9140-7C37-9639-EB8F44654F18",
  "excl_path": [
   "/cgroup/",
   "/dev/",
   "/etc/X11/xorg.conf/",
   "/etc/init.d/i2node/",
   "/etc/rc.d/init.d/i2node/",
   "/etc/sdata/",
   "/lost+found/",
   "/media/",
   "/mnt/",
   "/proc/",
   "/run/",
   "/selinux/",
   "/sys/",
   "/tmp/",
   "/usr/local/sdata/",
   "/var/i2/",
   "/var/i2data/",
   "/var/lock/",
   "/var/run/vmblock-fuse/"
  ],
  "random_str": "0DD4E727-70AB-62C6-BEB5-D012DFAE46E3",
  "fsp_wk_shut_flag": "2",
  "monitor_type": 0,
  "mirr_sync_attr": "1",
  "net_mapping_type": "2",
  "mirr_sync_flag": "0",
  "mirr_file_check": "0",
  "sync_item": "/",
  "secret_key": "",
  "failover": "2",
  "fsp_name": "linuxMove",
  "mirr_open_type": "0",
  "bk_uuid": "C11FE572-5207-3359-DB85-001E95F5F185",
  "bk_path": [
   "/",
   "/I2FFO/bin/",
   "/I2FFO/boot/",
   "/I2FFO/etc/",
   "/I2FFO/lib/",
   "/I2FFO/lib64/",
   "/I2FFO/root/",
   "/I2FFO/sbin/",
   "/I2FFO/usr/bin/",
   "/I2FFO/usr/lib/",
   "/I2FFO/usr/lib64/",
   "/I2FFO/usr/libexec/",
   "/I2FFO/usr/local/",
   "/I2FFO/usr/sbin/",
   "/I2FFO/var/lib/nfs/"
  ],
  "net_mapping": [],
  "service_uuid": "",
  "wk_uuid": "CE77F3D6-A6E3-A385-CE66-712313B7DDE8",
  "compress": "0",
  "encrypt_switch": "0",
  "move_type": "0",
  "wk_path": [
   "/",
   "/bin/",
   "/boot/",
   "/etc/",
   "/lib/",
   "/lib64/",
   "/root/",
   "/sbin/",
   "/usr/bin/",
   "/usr/lib/",
   "/usr/lib64/",
   "/usr/libexec/",
   "/usr/local/",
   "/usr/sbin/",
   "/var/lib/nfs/"
  ],
  "band_width": "3*03:00-16:00*2m"
 },
 "_": "3f513c82"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: fsp_move[compress_switch];\n The fsp_move[failover] field must be one of: 0,1;\n Lost: fsp_move[wk_data_type];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: moveFspMove
body: {
 "operate": "move",
 "fsp_uuids": [
  "3C6B932A-9140-7C37-9639-EB8F44654F18"
 ],
 "_": "3f41fde5"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist",
  "fail_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ],
  "all_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ]
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: rebootFspMove
body: {
 "operate": "move",
 "fsp_uuids": [
  "3C6B932A-9140-7C37-9639-EB8F44654F18"
 ],
 "_": "3f5fc456"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist",
  "fail_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ],
  "all_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ]
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: startFspMove
body: {
 "operate": "start",
 "fsp_uuids": [
  "3C6B932A-9140-7C37-9639-EB8F44654F18"
 ],
 "_": "3e4d62e7"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist",
  "fail_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ],
  "all_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ]
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: stopFspMove
body: {
 "operate": "stop",
 "fsp_uuids": [
  "3C6B932A-9140-7C37-9639-EB8F44654F18"
 ],
 "_": "3f4b606a"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist",
  "fail_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ],
  "all_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ]
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: verifyFspMoveLicense
body: {
 "wk_uuid": "CE77F3D6-A6E3-A385-CE66-712313B7DDE8",
 "bk_uuid": "C11FE572-5207-3359-DB85-001E95F5F185",
 "_": "3f0428f0"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010110022,
  "message": "[1010110022] Unable to get node hardware code."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: verifyFspMoveOldRule
body: {
 "wk_uuid": "CE77F3D6-A6E3-A385-CE66-712313B7DDE8",
 "bk_uuid": "C11FE572-5207-3359-DB85-001E95F5F185",
 "_": "3c2f6ff6"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: verifyFspMoveOsVersion
body: {
 "bk_uuid": "C11FE572-5207-3359-DB85-001E95F5F185",
 "wk_uuid": "CE77F3D6-A6E3-A385-CE66-712313B7DDE8",
 "_": "3f0e572d"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: mode;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: verifyFspMoveVolumeSpace
body: {
 "wk_uuid": "CE77F3D6-A6E3-A385-CE66-712313B7DDE8",
 "bk_uuid": "C11FE572-5207-3359-DB85-001E95F5F185",
 "sync_item": "/",
 "_": "3e9c94bf"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010011001,
  "message": "[1010011001] Node does not exist CE77F3D6-A6E3-A385-CE66-712313B7DDE8",
  "wk_vol_list": [],
  "bk_vol_list": [],
  "lost_volume": []
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: rebootFspMove
body: {
 "operate": "reboot",
 "fsp_uuids": [
  "3C6B932A-9140-7C37-9639-EB8F44654F18"
 ],
 "_": "3e322520"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist",
  "fail_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ],
  "all_list": [
   {
    "code": 1010001011,
    "message": "[1010001011] Item does not exist"
   }
  ]
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: verifyFspMoveEnvironment
body: {
 "wk_uuid": "",
 "bk_uuid": "",
 "wk_path": [
  {
   "": ""
  }
 ],
 "bk_path": [
  {
   "": ""
  }
 ],
 "_": "3dc8383b"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: wk_uuid;\n Lost: bk_uuid;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: batchCreateFspMove
body: {
 "base_info_list": {
  "service_uuid": "",
  "monitor_type": 0,
  "compress": 0,
  "net_mapping": [
   {
    "bk_nic": {
     "name": "Ethernet0",
     "type": "0",
     "ip": "192.168.72.74/255.255.240.0"
    },
    "wk_nic": {
     "name": "Ethernet0",
     "type": "0",
     "ip": "192.168.72.73/255.255.240.0"
    }
   }
  ],
  "encrypt_switch": 0,
  "mirr_open_type": "0",
  "mirr_sync_flag": "0",
  "net_mapping_type": "2",
  "mirr_sync_attr": "1",
  "band_width": "",
  "fsp_wk_shut_flag": 2,
  "secret_key": "",
  "mirr_file_check": "0",
  "failover": 0,
  "excl_driver": [
   "inf1",
   "inf2"
  ],
  "compress_switch": 1,
  "encrypt": 1,
  "wk_data_type": 1,
  "auto_start": 1,
  "bkup_one_time": 1,
  "backup_type": ""
 },
 "common_params": {
  "batch_name": "",
  "rep_prefix": "",
  "rep_sufix": "",
  "variable_type": 0
 },
 "node_list": [
  {
   "bk_uuid": "",
   "excl_path": [],
   "bk_path": [],
   "wk_uuid": "",
   "wk_path": [],
   "proxy_uuid": "",
   "data_ip_uuid": "",
   "sync_item": "/"
  }
 ],
 "_": "3f1ef54a"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001008,
  "message": "[1010001008] Invalid Name"
 }
}
-----------------------------------------------------------END>

