<START-----------------------------------------------------------
Method: batchCreateRule
body: {
 "tgt_type": "sqlserver",
 "map_type": "db",
 "config": {
  "enable_cdc": 0,
  "start_rule_now": 1,
  "dump_thd": 1,
  "sync_mode": 1,
  "drop_old_tab": 1,
  "table_map": ""
 },
 "rule_list": [
  {
   "rule_name": "",
   "src_db_uuid": "",
   "tgt_db_uuid": "",
   "mirror_db_uuid": ""
  }
 ],
 "_": "3f40266b"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001009,
  "message": "[1010001009] Name exists"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: checkName
body: {
 "_": "398ff2c0"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": []
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: createRule
body: {
 "rule_name": "test",
 "src_db_uuid": "7B1BE386-4CB1-86AA-D39D-B644C2EADD57",
 "tgt_db_uuid": "CD52E44B-D25A-4CE3-126F-6F5A460731E4",
 "tgt_type": "sqlserver",
 "map_type": "table",
 "config": {
  "start_rule_now": 1,
  "table_map": [
   {
    "src_user": "1",
    "src_table": "2",
    "dst_user": "1",
    "dst_table": "2",
    "column": []
   }
  ],
  "enable_cdc": 0,
  "mirror_db_uuid": "",
  "sync_mode": 1,
  "dump_thd": 1,
  "drop_old_tab": 1
 },
 "_": "3f052bd2"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010110011,
  "message": "[1010110011] The node / virtual platform has no license associated with this feature."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: createTbCmp
body: {
 "tb_cmp_name": "ctt->ctt",
 "src_db_uuid": "4CA773F4-36E3-A091-122C-ACDFB2112C21",
 "tgt_db_uuid": "40405FD3-DB86-DC8A-81C9-C137B6FDECE5",
 "cmp_type": "user,table,db",
 "db_user_map": "{\"CTT\":\"CTT\"}",
 "filter_table": "[\u7528\u6237.\u8868\u540d]",
 "db_tb_map": "\u8868\u6620\u5c04",
 "dump_thd": 1,
 "rule_uuid": "CFd25fA3-b4F1-68Cd-cc63-71A32DF58255",
 "polices": "\"0|00:00",
 "policy_type": "one_time",
 "concurrent_table": [
  "hh.ww"
 ],
 "try_split_part_table": 0,
 "one_time": "2019-05-27 16:07:08",
 "repair": 0,
 "fix_related": 0,
 "_": "3dba6320"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1011110001,
  "message": "[1011110001] Call failed. Can't connect to a node , platform or Address",
  "uuid": "56365303-46E7-3A3A-FC17-1DC75679C6CD"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteRule
body: {
 "uuids": [],
 "_": "3eb5c98e"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteTbCmp
body: {
 "force": "",
 "uuids": "FFfEFEb1-B1Ea-5Ad8-316A-8dCE24768A13",
 "_": "3f5728b7"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeListRule
body: {
 "rule_uuid": "6FBC9EB9-A10A-E226-9F2B-A77B3CF1D337",
 "_": "3f23d8de"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeTbCmp
body: {
 "uuid": "9bbC4b9D-9cCb-1461-edaD-BA16B1ABEff2",
 "_": "3e9ffab3"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001011,
  "message": "[1010001011] Item does not exist"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeTbCmpErrorMsg
body: {
 "offset": 1,
 "limit": 10,
 "search_field": "",
 "search_value": "",
 "uuid": "91Fdad26-4efC-F1CA-13c8-fb82eeFf42cE",
 "start_time": "",
 "name": "",
 "owner": "admin",
 "_": "3f409e27"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "result_info": {
   "rows": [],
   "total": 0
  }
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeTbCmpResult
body: {
 "page": 1,
 "limit": 10,
 "search_field": "",
 "search_value": "",
 "uuid": "DD8eB893-41E1-bC49-5eD4-8f63fB6cC19D",
 "start_time": "",
 "_": "3efbe16a"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "head_info": {
   "total": null,
   "err": null,
   "end": null,
   "start": null,
   "use_time": null,
   "all_err": null
  },
  "result_info": {
   "rows": [],
   "total": null,
   "rule_related": 0
  }
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeTbCmpResuluTimeList
body: {
 "time_list": "b165Ec3d-9Ffd-4eB2-f7bE-9Fc2DEAD5c11",
 "uuid": "",
 "_": "3f133cbb"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listRule
body: {
 "page": 1,
 "limit": 10,
 "search_field": "",
 "search_value": "",
 "group_uuid": "",
 "where_args": {
  "rule_uuid": ""
 },
 "_": "3f50a539"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listRuleStatus
body: {
 "uuids": "",
 "_": "3b994dbe"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listTbCmp
body: {
 "page": 1,
 "limit": 10,
 "search_field": "",
 "search_value": "",
 "_": "3f01b54e"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": [],
  "total": 0
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listTbCmpResultTimeList
body: {
 "uuid": "",
 "_": "3ecc315b"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "total": 0,
  "time_list": []
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listTbCmpStatus
body: {
 "uuids": "c7E49666-E6C8-4C30-48C3-3f3EA6F2AbDB",
 "_": "3f6f9315"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": []
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyRule
body: {
 "table_map": [
  {
   "src_user": "1",
   "src_table": "2",
   "dst_user": "1",
   "dst_table": "2",
   "column": []
  }
 ],
 "rule_name": "test",
 "src_db_uuid": "7B1BE386-4CB1-86AA-D39D-B644C2EADD57",
 "tgt_db_uuid": "CD52E44B-D25A-4CE3-126F-6F5A460731E4",
 "tgt_type": "sqlserver",
 "map_type": "table",
 "config": {},
 "start_rule_now": 1,
 "enable_cdc": 0,
 "mirror_db_uuid": "",
 "sync_mode": 1,
 "dump_thd": 1,
 "drop_old_tab": 1,
 "_": "3e0f4ffc",
 "uuid": ""
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010110011,
  "message": "[1010110011] The node / virtual platform has no license associated with this feature."
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: restartTbCmp
body: {
 "operate": "cmp_restart",
 "tb_cmp_uuids": "4FE3d4e6-5b7E-9ACF-2F3D-2e791cf7eD53",
 "_": "3f709f1e"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: resumeRule
body: {
 "operate": "resume",
 "uuids": "",
 "_": "3eda11f4"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: startRule
body: {
 "operate": "start",
 "uuids": "",
 "_": "3f58c5ab"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: startScheduleRule
body: {
 "operate": "start_schedule",
 "uuids": "",
 "_": "3f779d0a"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: stopRule
body: {
 "operate": "stop",
 "uuids": "",
 "_": "3f1148f7"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: stopScheduleRule
body: {
 "operate": "stop_schedule",
 "uuids": "",
 "_": "3f2f23a8"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: stopTbCmp
body: {
 "operate": "cmp_stop",
 "tb_cmp_uuids": "4FE3d4e6-5b7E-9ACF-2F3D-2e791cf7eD53",
 "_": "3dd4c7e8"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success"
 }
}
-----------------------------------------------------------END>

