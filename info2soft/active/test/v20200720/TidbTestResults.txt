<START-----------------------------------------------------------
Method: createTidbRule
body: {
 "db_map": [
  {
   "dst_table": "",
   "src_table": ""
  }
 ],
 "part_load_balance": "",
 "kafka_time_out": "",
 "full_sync_mode": "auto",
 "db_set": {
  "binlog_format": "row",
  "binlog_row_image": "full",
  "default_storage_engine": "innoDB",
  "sync_binlog": "1",
  "innodb_flush_log": "2",
  "innodb_flush_method": "O_DIRECT",
  "max_allowed_packet": "52",
  "open_files_limit": "65535",
  "server_id": "123456",
  "expire_logs_days": "7",
  "nat_mode": 0,
  "ip": ""
 },
 "full_sync_set": {
  "start_lsn": 1,
  "support_ddl": 1,
  "change_tf_path": "",
  "tf_file_save_time": 7,
  "nat_mode": 0,
  "foreign_ip": "",
  "extraction": 0
 },
 "modify": "",
 "primary_db_one": "",
 "primary_map_type_one": "",
 "primary_map_one": "",
 "primary_db_two": "",
 "primary_map_type_two": "",
 "start_src_db_set": 0,
 "primary_map_two": "",
 "dst_db_set": {
  "binlog_format": "",
  "binlog_row_image": "",
  "default_storage_engine": "",
  "sync_binlog": "",
  "innodb_flush_log": "",
  "innodb_flush_method": "",
  "max_allowed_packet": "",
  "open_files_limit": "",
  "server_id": "",
  "expire_logs_days": "",
  "nat_mode": 1,
  "ip": ""
 },
 "dst_full_sync_set": {
  "start_lsn": 1,
  "support_ddl": 1,
  "change_tf_path": "",
  "tf_file_save_time": "",
  "nat_mode": "",
  "foreign_ip": "",
  "extraction": 0
 },
 "start_dst_db_set": 0,
 "mysql_name": 1,
 "src_db_uuid": " 1B1153F6-DAD9-BC39-888A-A743FCC208E5",
 "tgt_db_uuid": " D42BF707-C971-EEA9-521F-BB0F3F7A92FC",
 "tgt_type": "oracle",
 "start_rule_now": 0,
 "dbmap_topic": "",
 "map_type": "table",
 "tab_map": [
  {
   "src_table": "src_table",
   "dst_table": "dst_table",
   "src_db": "111",
   "dst_db": "222"
  }
 ],
 "full_sync": 0,
 "incre_sync": 1,
 "model_type": "1:0",
 "config": {
  "src_connect_user": "",
  "dst_connect_user": "",
  "binary_code": "hex",
  "table_change_info": 1,
  "etl_settings": {
   "etl_table": [
    {
     "oprType": "IRP",
     "table": "",
     "user": "",
     "process": "SKIP",
     "addInfo": ""
    }
   ]
  },
  "bw_settings": {
   "bw_limit": "\"12*00:00-13:00*40M,3*00:00-13:00*40M\""
  },
  "full_sync_settings": {
   "clean_user_before_dum": 0,
   "concurrent_table": [],
   "dump_thd": 1,
   "load_thd": 1,
   "existing_table": "drop_to_recycle",
   "try_split_part_table": 1
  },
  "inc_sync_ddl_filter": {
   "inc_sync_ddl_data": [
    "INDEX",
    "VIEW",
    "FUNCTION"
   ]
  },
  "dml_track": {
   "delcol": "",
   "drp": 1,
   "enable": 1,
   "tmcol": "",
   "urp": 1
  },
  "jointing": {
   "op": "",
   "table": "",
   "content": ""
  }
 },
 "save_json_text": "",
 "_": "3f68dd28"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1011110001,
  "message": "[1011110001] Call failed. Can't connect to a node , platform or Address"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: describeTidbRule
body: {
 "mysql_uuid": "",
 "_": "3da290b2"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "info_list": null
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listTidbRules
body: {
 "where_args": {
  "mysql_uuid": "803bEec8-BbbC-E2FC-fdba-fA127c6C8cdc"
 },
 "page": 1,
 "limit": 10,
 "search_field": "",
 "search_value": "",
 "_": "3dd979f0"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: search_field;\n Not array: where_args;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: listTidbStatus
body: {
 "_": "3f2959f8"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  Lost: uuids[0];\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: modifyTidbRule
body: {
 "config": {
  "kafka_time_out": "",
  "part_load_balance": ""
 },
 "mysql_name": "mysql",
 "src_db_uuid": " 1B1153F6-DAD9-BC39-888A-A743FCC208E5",
 "tgt_db_uuid": " D42BF707-C971-EEA9-521F-BB0F3F7A92FC",
 "tgt_type": "kafka",
 "start_rule_now": 0,
 "node_uuid": " 6B1153F6-DAD9-BC39-888A-A743FCC208E6",
 "dbmap_topic": "",
 "map_type": "table",
 "tab_map": [
  {
   "src_table": "src_table",
   "topic": "topic"
  }
 ],
 "full_sync": 0,
 "incre_sync": 1,
 "model_type": "1:0",
 "full_sync_mode": "auto",
 "db_set": {
  "db_node": "1B1153F6-DAD9-BC39-888A-A743FCC208E5",
  "binlog_format": "row",
  "binlog_row_image": "full",
  "default_storage_engine": "innoDB",
  "sync_binlog": "1",
  "innodb_flush_log": "2",
  "innodb_flush_method": "O_DIRECT",
  "max_allowed_packet": "52",
  "open_files_limit": "65535",
  "server_id": "123456",
  "expire_logs_days": "7",
  "nat_mode": 0,
  "ip": ""
 },
 "full_sync_set": {
  "support_ddl": 1,
  "node": " 6B1153F6-DAD9-BC39-888A-A743FCC208E6",
  "change_tf_path": "",
  "tf_file_save_time": 7,
  "nat_mode": 0,
  "foreign_ip": ""
 },
 "primary_node_one": "",
 "primary_node_two": "",
 "primary_db_one": "",
 "primary_map_type_one": "",
 "primary_map_one": [],
 "primary_db_two": "",
 "primary_map_type_two": "",
 "primary_map_two": [],
 "db_map": [
  {
   "src_db": "src_db",
   "dst_db": "dst_db"
  }
 ],
 "mysql_uuid": "5349E2CF-7DBO-OAF2-13CB-BB7DFD8A9D86",
 "_": "3ee180fe"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1011110001,
  "message": "[1011110001] Call failed. Can't connect to a node , platform or Address"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: removeTidbRule
body: {
 "tf": "",
 "operate": "remove",
 "mysql_uuid": "48CdCFB0-4Ed4-bee0-3AC7-2A280b8dbD9C",
 "scn": "",
 "_": "3f5fc9b3"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  The operate field must be one of: resume,stop,restart,start_schedule,stop_schedule;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: resetLoadTidbRule
body: {
 "tf": "",
 "operate": "reset_load",
 "mysql_uuid": "48CdCFB0-4Ed4-bee0-3AC7-2A280b8dbD9C",
 "scn": "",
 "_": "3f78a866"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  The operate field must be one of: resume,stop,restart,start_schedule,stop_schedule;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: resetParsingTidbRule
body: {
 "tf": "",
 "operate": "reset_parsing",
 "mysql_uuid": "48CdCFB0-4Ed4-bee0-3AC7-2A280b8dbD9C",
 "scn": "",
 "_": "3e794def"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  The operate field must be one of: resume,stop,restart,start_schedule,stop_schedule;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: restartTidbRule
body: {
 "tf": "",
 "operate": "restart",
 "mysql_uuid": "48CdCFB0-4Ed4-bee0-3AC7-2A280b8dbD9C",
 "scn": "",
 "_": "3f6a33e2"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1011110001,
  "message": "[1011110001] Call failed. Can't connect to a node , platform or Address"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: resumeTidbRule
body: {
 "tf": "",
 "operate": "resume",
 "mysql_uuid": "48CdCFB0-4Ed4-bee0-3AC7-2A280b8dbD9C",
 "scn": "",
 "_": "3ec1d0f9"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1011110001,
  "message": "[1011110001] Call failed. Can't connect to a node , platform or Address"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: startLoadTidbRule
body: {
 "tf": "",
 "operate": "start_load",
 "mysql_uuid": "48CdCFB0-4Ed4-bee0-3AC7-2A280b8dbD9C",
 "scn": "",
 "_": "3f49d66b"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  The operate field must be one of: resume,stop,restart,start_schedule,stop_schedule;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: startParsingTidbRule
body: {
 "tf": "",
 "operate": "start_parsing",
 "mysql_uuid": "48CdCFB0-4Ed4-bee0-3AC7-2A280b8dbD9C",
 "scn": "",
 "_": "3ea3ec56"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  The operate field must be one of: resume,stop,restart,start_schedule,stop_schedule;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: stopLoadTidbRule
body: {
 "tf": "",
 "operate": "stop_load",
 "mysql_uuid": "48CdCFB0-4Ed4-bee0-3AC7-2A280b8dbD9C",
 "scn": "",
 "_": "3bf447a3"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  The operate field must be one of: resume,stop,restart,start_schedule,stop_schedule;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: stopParsingTidbRule
body: {
 "tf": "",
 "operate": "stop_parsing",
 "mysql_uuid": "48CdCFB0-4Ed4-bee0-3AC7-2A280b8dbD9C",
 "scn": "",
 "_": "3f713461"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1010001002,
  "message": "[1010001002] Invalid Parameter  The operate field must be one of: resume,stop,restart,start_schedule,stop_schedule;\n"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: stopTidbRule
body: {
 "tf": "",
 "operate": "stop",
 "mysql_uuid": "48CdCFB0-4Ed4-bee0-3AC7-2A280b8dbD9C",
 "scn": "",
 "_": "3f635f3a"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 1011110001,
  "message": "[1011110001] Call failed. Can't connect to a node , platform or Address"
 }
}
-----------------------------------------------------------END>

<START-----------------------------------------------------------
Method: deleteTidbRule
body: {
 "force": 1,
 "mysql_uuids": [
  "befDCd02-dbBB-05fb-3788-B192EDc6c46E"
 ],
 "_": "3f5865a1"
}
response: {
 "ret": 200,
 "msg": "",
 "data": {
  "code": 0,
  "message": "[0] success",
  "success_list": [
   {
    "code": 0,
    "message": "[0] success"
   }
  ],
  "all_list": [
   {
    "code": 0,
    "message": "[0] success"
   }
  ]
 }
}
-----------------------------------------------------------END>

